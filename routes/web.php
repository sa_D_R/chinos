<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//primera ruta 
Route::get('hola',function(){
    echo"hola estoy el laravel";
});

Route::get('arreglo',function(){
    //crear un arreglo de estudiantes
    $estudiantes = ["AD"=>"andres",
                    "LA"=>"laura",
                    "AN"=>"ana",
                    "RO"=>"rodrigo"
                    ];
    //var_dump($estudiantes);

    //recorrer un areglo
    foreach($estudiantes as  $indice => $estudiante){
        echo "$estudiante tiene indice $indice <hr />";  //br es un salto de linea
    }

});

Route::get('paises',function(){
    //crear un arreglo con informacion de paises
    $paises =[
        "colombia" => [
                      "capital"=>"bogota",
                      "moneda"=>"peso",
                      "poblacion"=>50.372      
                    ],
        "ecuador" => [
                     "capital"=>"quito",
                      "moneda"=>"dolar",
                      "poblacion"=>17.517 
                     ],
        "brazil" => [
                     "capital"=>"brasilia",
                     "moneda"=>"real",
                     "poblacion"=>212.216 
                    ],
        "bolivia" => [
                     "capital"=>"la paz",
                     "moneda"=>"boliviano",
                     "poblacion"=>11.633 
                     ]
         ];
    //echo "<pre>";
    //var_dump($paises);
    //echo"</pre>";



    //recorrer la primera dmencion del arreglo
   // foreach($paises as $pais =>$infopais){
    //   echo "<h2> $pais </h2>";
        
     //   echo "capital : " . $infopais["capital"] . "<br/>";
       // echo "moneda : " . $infopais["moneda"] . "<br/>";
       // echo "poblacion : (en millones de avitantes ) " . $infopais["poblacion"] . "<br/>";
        //echo "<hr/>";
    //}


    //mostrar una vista para presentar los paises
    //en mvc yo puedo pasar datos a una vista 
    return view("paises") -> with("paises" , $paises);


});